

INSERT INTO franchise (name, description) VALUES ('Marvel Cinematic Universe', 'A series of superhero films produced by Marvel Studios');
INSERT INTO movie (title, release_date, franchise_id) VALUES ('Iron Man', '2008-05-02', 1);
INSERT INTO movie (title, release_date, franchise_id) VALUES ('The Avengers', '2012-05-04', 1);
INSERT INTO character (name, alias, gender, picture_url, movie_id) VALUES ('Tony Stark', 'Iron Man', 'Male', 'https://example.com/ironman.jpg', 1);
INSERT INTO character (name, alias, gender, picture_url, movie_id) VALUES ('Steve Rogers', 'Captain America', 'Male', 'https://example.com/captainamerica.jpg', 2);