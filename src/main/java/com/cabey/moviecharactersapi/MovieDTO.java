package com.cabey.moviecharactersapi;


import java.util.List;

public class MovieDTO {

    private Long id;
    private String title;
    private String releaseDate;
    private List<CharacterDto> characters;

    // Getters and setters
}