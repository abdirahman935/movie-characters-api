package com.cabey.moviecharactersapi;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/franchises")
public class FranchiseController {

    @Autowired
    @Service
    private FranchiseService franchiseService;

    @GetMapping("/{id}")
    public ResponseEntity<FranchiseDTO> getFranchiseById(@PathVariable Long id) {
        FranchiseDTO franchise = franchiseService.getFranchiseById(id);
        if (franchise == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(franchise);
    }

    @PostMapping
    public ResponseEntity<FranchiseDTO> createFranchise(@RequestBody FranchiseDTO franchiseDTO) {
        FranchiseDTO createdFranchise = franchiseService.createFranchise(franchiseDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdFranchise);
    }

    @PutMapping("/{id}")
    public ResponseEntity<FranchiseDTO> updateFranchise(@PathVariable Long id, @RequestBody FranchiseDTO franchiseDTO) {
        FranchiseDTO updatedFranchise = franchiseService.updateFranchise(id, franchiseDTO);
        if (updatedFranchise == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedFranchise);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFranchise(@PathVariable Long id) {
        boolean deleted = franchiseService.deleteFranchise(id);
        if (deleted) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/{id}/movies")
    public ResponseEntity<List<MovieDTO>> getMoviesByFranchiseId(@PathVariable Long id) {
        List<MovieDTO> movies = franchiseService.getMoviesByFranchiseId(id);
        return ResponseEntity.ok(movies);
    }

    @GetMapping("/{id}/characters")
    public ResponseEntity<List<CharacterDTO>> getCharactersByFranchiseId(@PathVariable Long id) {
        List<CharacterDTO> characters = franchiseService.getCharactersByFranchiseId(id);
        return ResponseEntity.ok(characters);
    }

}
