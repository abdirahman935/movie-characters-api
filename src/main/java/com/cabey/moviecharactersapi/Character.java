package com.cabey.moviecharactersapi;

import jakarta.persistence.*;
import org.hibernate.annotations.Table;
import com.cabey.moviecharactersapi.Movie;

import java.util.List;

// i have used JPA annotations such as @Entity, @Table, @Id, @Column, @GeneratedValue, @ManyToOne, @OneToMany, and @JoinColumn to define the relationships
// between the entities and their attributes. These annotations help Hibernate to generate the necessary SQL statements to create
// the database schema and perform CRUD operations on the entities.

@Entity
//@Table(name = "character")
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "alias")
    private String alias;

    @Column(name = "gender")
    private String gender;

    @Column(name = "picture_url")
    private String pictureUrl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_id")
    private Movie movie;

    // Getters and setters omitted for brevity
}
