package com.cabey.moviecharactersapi;
import jakarta.persistence.*;
import org.hibernate.annotations.Table;

import com.cabey.moviecharactersapi.Movie;
import java.util.List;
@Entity
//@Table(name = "franchise")

public class Franchise {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "name")
        private String name;

        @Column(name = "description")
        private String description;

        @OneToMany(mappedBy = "franchise", cascade = CascadeType.ALL, orphanRemoval = true)
        private List<Movie> movies;

        // Getters and setters omitted for brevity
    }

