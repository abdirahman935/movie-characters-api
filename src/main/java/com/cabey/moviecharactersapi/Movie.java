package com.cabey.moviecharactersapi;
import jakarta.persistence.*;
import org.hibernate.annotations.Table;
import com.cabey.moviecharactersapi.Franchise;

import java.util.List;

@Entity
//@Table(name = "movie")
public class Movie {



@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

@Column(name = "title")
private String title;

@Column(name = "release_date")
private String releaseDate;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "franchise_id")
private Franchise franchise;

@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, orphanRemoval = true)
private List<Character> characters;

// Getters and setters omitted for brevity
}


