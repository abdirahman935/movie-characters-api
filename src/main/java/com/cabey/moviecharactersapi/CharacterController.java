package com.cabey.moviecharactersapi;
import com.cabey.moviecharactersapi.CharacterDto;
import com.cabey.moviecharactersapi.CharacterMapper;
import org.springframework.beans.factory.annotation.Autowired;
;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@RestController
@Controller
@RequestMapping("/characters")
public class CharacterController {
    @Autowired
    private CharacterService characterService;

    @GetMapping("/{id}")
    public CharacterDto getCharacterById(@PathVariable Long id) {
        Character character = characterService.getCharacterById(id);
        return CharacterMapper.toDto(character);
    }

    @PostMapping
    public CharacterDto createCharacter(@RequestBody CharacterDto characterDto) {
        Character character = CharacterMapper.toEntity(characterDto);
        Character savedCharacter = characterService.saveCharacter(character);
        return CharacterMapper.toDto(savedCharacter);
    }

    @PutMapping("/{id}")
    public CharacterDto updateCharacter(@PathVariable Long id, @RequestBody CharacterDto characterDto) {
        Character character = CharacterMapper.toEntity(characterDto);
        character.setId(id);
        Character updatedCharacter = characterService.updateCharacter(character);
        return CharacterMapper.toDto(updatedCharacter);
    }

    @DeleteMapping("/{id}")
    public void deleteCharacter(@PathVariable Long id) {
        characterService.deleteCharacterById(id);
    }
}

@Service
public class CharacterService {
    @Autowired
    private CharacterRepository characterRepository;

    public Character getCharacterById(Long id) {
        return characterRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Character not found"));
    }

    public Character saveCharacter(Character character) {
        return characterRepository.save(character);
    }

    public Character updateCharacter(Character character) {
        return characterRepository.save(character);
    }

    public void deleteCharacterById(Long id) {
        characterRepository.deleteById(id);
    }
}

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
    // add custom methods here if needed
}