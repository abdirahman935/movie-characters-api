package com.cabey.moviecharactersapi;
import com.cabey.moviecharactersapi.Character;

public class CharacterMapper {
    public static CharacterDto toDto(Character character) {
        CharacterDto characterDto = new CharacterDto();
        characterDto.setId(character.getId());
        characterDto.setFullName(character.getFullName());
        characterDto.setAlias(character.getAlias());
        characterDto.setGender(character.getGender());
        characterDto.setPictureUrl(character.getPictureUrl());
        return characterDto;
    }

    public static Character toEntity(CharacterDto characterDto) {
        Character character = new Character();
        character.setId(characterDto.getId());
        character.setFullName(characterDto.getFullName());
        character.setAlias(characterDto.getAlias());
        character.setGender(characterDto.getGender());
        character.setPictureUrl(characterDto.getPictureUrl());
        return character;
    }
}